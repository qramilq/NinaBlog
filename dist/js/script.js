$( document ).ready(function() {
    console.log( "ready!" );
    $('#mixxx').mixItUp({
    	controls: {
	        // enable: true,
	        // live: false,
	        // toggleFilterButtons: true,
	        // toggleLogic: 'and',
	        activeClass: 'active'
	    },
	    load: {
	    	sort:'added:asc'
	    }  
    });
    $('.sort-btn').on('click', function(){
		var dataSortSplit = $(this).attr('data-sort').split(':');
		var dataSort = dataSortSplit[1] == 'asc' ? dataSortSplit[0] + ':desc' : dataSortSplit[0] + ':asc';
		$(this).attr('data-sort', dataSort)
		// $(this).html($(this).html() + ' ' + dataSort)
		$(this).siblings().removeClass('sort-asc sort-desc');
		if (dataSortSplit[1] == 'asc'){
			$(this).removeClass('sort-asc');
			$(this).addClass('sort-desc');
		} else {
			$(this).removeClass('sort-desc');
			$(this).addClass('sort-asc');
		}
		$('#mixxx').mixItUp('sort', dataSort);
	});

});